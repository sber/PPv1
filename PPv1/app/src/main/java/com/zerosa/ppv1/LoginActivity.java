package com.zerosa.ppv1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity
{
    public Button log_bttn;
    public ImageButton cancel_bttn;
    public EditText ed1,ed2;
    public TextView myTextView2,msg_2_close_app;

    public ListView lstOpciones;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String sel_usr = "sel_usr";


    public TextView lblMensaje;
    public ImageButton pinButton;
    
    //Variables para DB y su gestion
        ContentValues valores= new ContentValues();
        Cursor db_cur;
        String[] campos;
        UsuariosSQLiteHelper usdbh;
        SQLiteDatabase db;
        ArrayList<String> list = new ArrayList<String>();



    Intent i;
    TextView tx1;
    int err_counter = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);



        //Creo tabla de usuarios
            usdbh = new UsuariosSQLiteHelper(this, "DBusrs", null, 1);

            db = usdbh.getWritableDatabase();

            //Me aseguro que no vuelva a  escribir la tabla si ya está creada
            db_cur = db.query("app_usrs", campos, null, null, null, null, null);
            if(!db_cur.moveToFirst())
            {
                //Genero los valores
                    valores.put("usr", "admin");
                    valores.put("pass", "admin");
                    db.insert("app_usrs", null, valores);
                    valores.put("usr", "usuario1");
                    valores.put("pass", "usuario1");
                    db.insert("app_usrs", null, valores);
            }
            campos = new String[]{"usr", "pass"};
            db_cur = db.query("app_usrs", campos, null, null, null, null, null);
        if (db_cur.moveToFirst())
        {
            //Recorremos el cursor hasta que no haya más registros
            do {
                list.add(db_cur.getString(0));
                list.add(db_cur.getString(1));
            } while (db_cur.moveToNext());
        }

        log_bttn = (Button)findViewById(R.id.log_bttn);
        ed1 = (EditText)findViewById(R.id.editText);
        ed2 = (EditText)findViewById(R.id.editText2);

        cancel_bttn = (ImageButton)findViewById(R.id.cancel_bttn);
        tx1 = (TextView)findViewById(R.id.textView3);
        tx1.setVisibility(View.GONE);

        myTextView2 = (TextView)findViewById(R.id.textView2);
        msg_2_close_app = (TextView)findViewById(R.id.msg_cierre_app);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        log_bttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor usr_sel = sharedpreferences.edit();
                if(ed1.getText().toString().equals(list.get(0)) && ed2.getText().toString().equals(list.get(1)))
                {
                    Toast.makeText(getApplicationContext(),
                            "Bienvenido a la APP admin...",Toast.LENGTH_SHORT).show();

                    //Guardo el item seleccionado en las shared preferences
                        usr_sel.putString(sel_usr, list.get(0));
                        usr_sel.commit();


                    //Call Main activity
                        i=new Intent(LoginActivity.this, com.zerosa.ppv1.MainActivity.class);
                        startActivity(i);

                }
                else if(ed1.getText().toString().equals(list.get(2)) && ed2.getText().toString().equals(list.get(3)))
                {
                    Toast.makeText(getApplicationContext(),
                            "Bienvenido a la APP usuario1...",Toast.LENGTH_SHORT).show();
                    //Guardo el item seleccionado en las shared preferences
                        usr_sel.putString(sel_usr, list.get(2));
                        usr_sel.commit();
                    //Call Main activity
                        i=new Intent(LoginActivity.this, com.zerosa.ppv1.MainActivity.class);
                        startActivity(i);
                }
                else{
                    Toast.makeText(getApplicationContext(), "Datos incorrectos",Toast.LENGTH_SHORT).show();

                            tx1.setVisibility(View.VISIBLE);
                    tx1.setBackgroundColor(Color.RED);
                    err_counter--;
                    tx1.setText(Integer.toString(err_counter));
                    myTextView2.setText("Intentos posibles:");

                    if (err_counter == 0) {
                        log_bttn.setEnabled(false);
                        msg_2_close_app.setText("Cierre la APP y vuelva a ingresar");
                        msg_2_close_app.setTextColor(Color.BLUE);
                    }
                }
            }
        });

        cancel_bttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });

       // this.deleteDatabase("DBusrs");
        //db.close();
    }

/*
    //Borro la base de datos al finalizar el programa asi no acumula valores
    @Override
    public void onDestroy()
    {

        super.onDestroy();
        /*
         *Borro
         *   Tabla ( como borro la base puedo obviar este paso)
         *   Base de datos
         *   para que no se acumulen los valores y cada vez que cierro la app empieze desde 0, sin tener que
         *   actualizar la versión. ES IMPORTANTE RESPETAR EL ORDEN DE LAS INSTRUCCIONES EN ESTA
         *   SECUENCIA!!!!
         *

        //db.execSQL("DROP TABLE IF EXISTS Usuarios");
        this.deleteDatabase("DBusrs");
        db.close();

    }
*/
}



