package com.zerosa.ppv1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;

public class PrefsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener{

    //Gestion de sharedpreferences
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String last_pressed_back_col = "prssd_col";
    public static final String bckgr_col = "col_key";


    Intent returnIntent = new Intent();




    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        // inflates our preference resource file into the fragment, displaying our Settings
            addPreferencesFromResource(R.xml.settings_view);

        //Declaro el estado inicial de los chk_buttons
            CheckBoxPreference chk_white = (CheckBoxPreference) findPreference("white");
            CheckBoxPreference chk_green = (CheckBoxPreference) findPreference("green");
            CheckBoxPreference chk_gray = (CheckBoxPreference) findPreference("gray");
            CheckBoxPreference chk_blue = (CheckBoxPreference) findPreference("blue");

        //Recupero el color seleccionado
            SharedPreferences prefs = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String pressed_item = prefs.getString(last_pressed_back_col,"white");

        //Marco el checkbox correspondiente.Como hago un finish al final siempre entro en este modo
            switch(pressed_item)
            {
                case "white":
                    chk_white.setChecked(true);
                    chk_green.setChecked(false);
                    chk_gray.setChecked(false);
                    chk_blue.setChecked(false);
                    break;

                case "green":
                    chk_white.setChecked(false);
                    chk_green.setChecked(true);
                    chk_gray.setChecked(false);
                    chk_blue.setChecked(false);
                    break;

                case "gray":
                    chk_white.setChecked(false);
                    chk_green.setChecked(false);
                    chk_gray.setChecked(true);
                    chk_blue.setChecked(false);
                    break;

                case "blue":
                    chk_white.setChecked(false);
                    chk_green.setChecked(false);
                    chk_gray.setChecked(false);
                    chk_blue.setChecked(true);
                    break;
            }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        //Genero el editor de las sharedpreferences
            sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor settings = sharedpreferences.edit();

        CheckBoxPreference chk_white = (CheckBoxPreference) findPreference("white");
        CheckBoxPreference chk_green = (CheckBoxPreference) findPreference("green");
        CheckBoxPreference chk_gray = (CheckBoxPreference) findPreference("gray");
        CheckBoxPreference chk_blue = (CheckBoxPreference) findPreference("blue");



        switch (key)
        {
            case "white":
                settings.putString(bckgr_col,"white");
                settings.commit();
                break;

            case "green":
                settings.putString(bckgr_col,"green");
                settings.commit();
                break;

            case "gray":
                settings.putString(bckgr_col,"gray");
                settings.commit();
                break;

            case "blue":
                settings.putString(bckgr_col,"blue");
                settings.commit();
                break;


        }
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        getActivity().finish();
    }

}