package com.zerosa.ppv1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        //creating new thread just for demonstration of background tasks
        Thread t=new Thread()
        {
            public void run()
            {

                try
                {
                    //sleep thread for 3 seconds
                    sleep(3000);

                    //Call Main activity
                    Intent i=new Intent(SplashActivity.this, com.zerosa.ppv1.LoginActivity.class);
                    startActivity(i);

                    //destroying Splash activity
                    finish();

                }

                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        //start thread
        t.start();
    }
}
