package com.zerosa.ppv1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.SharedPreferences;




public class TabFragment1 extends Fragment
{
    public TextView tab1_txt;



    //variables para llenar la db
        int value,i=0;
        String name,storage;
        UsuariosSQLiteHelper usdbh;
        SQLiteDatabase db;
        ContentValues valores= new ContentValues();
        ContentValues nuevoRegistro = new ContentValues();
        Cursor db_cur;
        String[] campos;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";

    public TabFragment1()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.tab_view1, container, false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void onActivityCreated(Bundle state)
    {
        super.onActivityCreated(state);
        tab1_txt = (TextView)getView().findViewById(R.id.tab1_txt);

        //Recupero el item seleccionado de las sharedpreferences
            SharedPreferences prefs = this.getActivity().getSharedPreferences(MyPREFERENCES, getContext().MODE_PRIVATE);
            String pressed_item = prefs.getString(Name,null);

        //Leo los valores de la base de datos
            usdbh = new UsuariosSQLiteHelper(getContext(), "DBUsuarios", null, 1);
            db = usdbh.getWritableDatabase();
            campos = new String[]{"value", "name", "storage"};
            String[] args =new String[]{ pressed_item};
            db_cur = db.query("Usuarios", campos, "name=?", args, null, null, null);
            db_cur.moveToFirst();

        //Inserto el valor en el fragment
            tab1_txt.setText("$"+String.valueOf(db_cur.getInt(0)));
    }

}