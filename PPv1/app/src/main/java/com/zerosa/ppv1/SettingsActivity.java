package com.zerosa.ppv1;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceActivity;


//Esta Activity es la que contiene al fragment, que a su vez va  contener las settings
public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();
    }

}