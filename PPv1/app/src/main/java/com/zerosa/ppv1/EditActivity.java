package com.zerosa.ppv1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity
{
    public TextView edit_view;
    public Button bttn_edit,bttn_create;
    public ImageButton bttn_add,bttn_sett,bttn_edit_psw;
    public EditText txt_stg_edit,txt_val_edit,txt_name_edit;
    String pressed_item,new_stg,new_name;
    int new_val;
    Bundle extras;
    Intent returnIntent = new Intent();


    //variables para llenar la db
        UsuariosSQLiteHelper usdbh;
        SQLiteDatabase db;
        ContentValues valores= new ContentValues();
        Cursor db_cur;
        String[] campos;
        String[] args;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        extras = getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_view);
        txt_stg_edit = (EditText) findViewById(R.id.txt_stg_edit);
        txt_val_edit = (EditText) findViewById(R.id.txt_val_edit);
        txt_name_edit = (EditText) findViewById(R.id.txt_name_edit);
        edit_view = (TextView) findViewById(R.id.edit_view);
        bttn_edit =(Button) findViewById(R.id.bttn_edit);
        bttn_create =(Button) findViewById(R.id.bttn_create);
        //Escondo los botones del Toolbar que no deseo ver
            bttn_add = (ImageButton) findViewById(R.id.bttn_add);
            bttn_add.setVisibility(View.GONE);
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setVisibility(View.GONE);
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
            bttn_edit_psw.setVisibility(View.GONE);

        //Abro la base de datos para buscar los valores por que ya estaban cargados
            usdbh = new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);
            db = usdbh.getWritableDatabase();
            campos = new String[]{"value", "name", "storage"};
            args =new String[]{ extras.getString("ferraris_name")};




        if (extras.getBoolean("action_flag"))
        {
            //Agrego un item
                bttn_create.setVisibility(View.VISIBLE);
                txt_name_edit.setVisibility(View.VISIBLE);
                edit_view.setVisibility(View.GONE);
                bttn_edit.setVisibility(View.GONE);


                bttn_create.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        //Genero mensaje si no se completó un campo
                            if (txt_val_edit.getText().toString().equals("")||txt_name_edit.getText().toString().equals("")||txt_stg_edit.getText().toString().equals(""))
                                Toast.makeText(getApplicationContext(),"La Ferrari no fue agregada, porfavor complete todos los campos",Toast.LENGTH_SHORT).show();

                        else
                        {

                            //Me fijo si existe el elemento dummy, y lo borro si está
                                String[] args = new String[]{"dummy_Ferrari"};
                                db_cur = db.query("Usuarios", campos,  null,null, null, null, null);
                                db_cur.moveToFirst();

                                if(db_cur.getString(1).equals("dummy_Ferrari"))
                                    db.delete("Usuarios", "name=?", args);

                            new_val = Integer.valueOf(txt_val_edit.getText().toString());
                            new_name =txt_name_edit.getText().toString();
                            new_stg =txt_stg_edit.getText().toString();


                            //Almaceno los nuevos valores
                                ContentValues valores= new ContentValues();
                                valores.put("value",new_val);
                                valores.put("name",new_name);
                                valores.put("storage",new_stg);

                            //Inserto los valores en la tabla
                             db.insert("Usuarios", null, valores);
                        }
                        //Retorno al main activity
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                    }
                });

        }
        else if(!extras.getBoolean("action_flag"))
        {
            //Edito un item
                edit_view.setVisibility(View.VISIBLE);
                bttn_edit.setVisibility(View.VISIBLE);
                bttn_create.setVisibility(View.GONE);
                txt_name_edit.setText(extras.getString("ferraris_name"));
                txt_name_edit.setVisibility(View.GONE);

            //Cargo los valores en el cursor
                db_cur = db.query("Usuarios", campos, "name=?", args, null, null, null);
                db_cur.moveToFirst();


            bttn_edit.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    //Verifico si se ingresan datos en caso que no se ingresen dejo el valor anterior
                        if(txt_val_edit.getText().toString().equals(""))
                            new_val=db_cur.getInt(0);
                        else
                            new_val = Integer.valueOf(txt_val_edit.getText().toString());


                        if(txt_stg_edit.getText().toString().equals(""))
                            new_stg=db_cur.getString(2);
                        else
                            new_stg = txt_stg_edit.getText().toString();

                    //Cargo los valores editados
                        ContentValues valores_edit= new ContentValues();
                        valores_edit.put("value",new_val);
                        String[] args = new String[]{extras.getString("ferraris_name")};
                        valores_edit.put("storage",new_stg);

                        //Actualizo el registro en la base de datos
                            db.update("Usuarios", valores_edit, "name=?" , args);
                    //Retorno al main activity
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                }

            });

        }

    }
}

