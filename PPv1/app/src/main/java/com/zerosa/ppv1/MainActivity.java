package com.zerosa.ppv1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ListView;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import java.util.ArrayList;
import android.content.SharedPreferences;



public class MainActivity extends AppCompatActivity
{

    //Gestion de sharedpreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String Name = "nameKey";
        public static final String last_pressed_back_col = "prssd_col";
        public static final String bckgr_col = "col_key";




    //Variable para checkean el boton correspondiente en settings
        String pressed_Col="white";//valor por default blanco


    public ImageButton bttn_add,bttn_sett,bttn_edit_psw;



    //Herramientas para gestion y visualizacion de la base de datos
    public ListView lstOpciones;
        ArrayList<String> list = new ArrayList<String>();
        ArrayAdapter<String> myAdapter;
        ContentValues valores= new ContentValues();
        ContentValues nuevoRegistro = new ContentValues();
        Cursor db_cur;
        String[] campos;
        //variables para llenar la db
            int value,i=0;
            String name,storage;


    UsuariosSQLiteHelper usdbh;

    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        //Configuro el toolbar y sus botones

            //Asigno el toolbar a la activity
                Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
                    setSupportActionBar(toolbar);

            //Configuro el botton para agregar
                 bttn_add = (ImageButton) findViewById(R.id.bttn_add);
                    bttn_add.setOnClickListener(new View.OnClickListener()
                    {
                        public void onClick(View v)
                        {

                            Intent edit_intent = new Intent(MainActivity.this, EditActivity.class);
                            edit_intent.putExtra("action_flag",true);//id;value
                            startActivityForResult(edit_intent,2);//intent_id;request_code

                        }

                    });


            //Configuro el botton para ir a las configuraciones
                bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
                    bttn_sett.setOnClickListener(new View.OnClickListener()
                        {
                        public void onClick(View v)
                        {
                            SharedPreferences.Editor settings = sharedpreferences.edit();
                            //Guardo el item seleccionado en las shared preferences
                            settings.putString(last_pressed_back_col, pressed_Col);
                            settings.commit();
                            Intent sett_intent = new Intent(MainActivity.this, SettingsActivity.class);
                            startActivityForResult(sett_intent,3);//intent_id;request_code

                            }

                     });


        //Configuro el botton para ir al cambio de contraseña
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
                bttn_edit_psw.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View v)
                    {

                        Intent psw_intent = new Intent(MainActivity.this, PasswordActivity.class);
                        startActivity(psw_intent);//intent_id;

                    }

                });

        //Comienzo con gestion de BD
            //Creo la lista a llenar con los items de la BD
                lstOpciones = (ListView) findViewById(R.id.LstOpciones);

            //Abro la BD
                usdbh = new UsuariosSQLiteHelper(this, "DBUsuarios", null, 1);
                db = usdbh.getWritableDatabase();

            //Me aseguro que no vuelva a  escribir la tabla si ya está creada
                db_cur = db.query("Usuarios", campos, null, null, null, null, null);
                if(!db_cur.moveToFirst())
                {
                //Genero los valores iniciales
                    nuevoRegistro.put("value", 3000000);
                    nuevoRegistro.put("name", "Ferrari 250");
                    nuevoRegistro.put("storage", "Alemania");
                    //Insertamos el registro en la base de datos
                    db.insert("Usuarios", null, nuevoRegistro);
                    nuevoRegistro.put("value", 3500000);
                    nuevoRegistro.put("name", "Ferrari 250 GT Berlinetta");
                    nuevoRegistro.put("storage", "Francia");
                    db.insert("Usuarios", null, nuevoRegistro);
                    nuevoRegistro.put("value", 4000000);
                    nuevoRegistro.put("name", "Ferrari 250 GTO");
                    nuevoRegistro.put("storage", "Italia");
                    db.insert("Usuarios", null, nuevoRegistro);
                    nuevoRegistro.put("value", 2500000);
                    nuevoRegistro.put("name", "Ferrari 458 Speciale");
                    nuevoRegistro.put("storage", "España");
                    db.insert("Usuarios", null, nuevoRegistro);
                }

                //Cargo los valoes de la BD en su cursor para pasarlos al Arraylist
                    campos = new String[]{"value", "name", "storage"};
                    db_cur = db.query("Usuarios", campos, null, null, null, null, null);

                //Nos aseguramos de que existe al menos un registro, recorro al cursor hasta que no haya más registros
                    if (db_cur.moveToFirst())
                    {
                        do
                        {
                            list.add(db_cur.getString(1));
                        } while (db_cur.moveToNext());
                    }

                //Creo el adaptador de la lista y le paso los elementos
                    myAdapter =new ArrayAdapter<String>(this, R.layout.listitem_titular, R.id.LblTitulo, list) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent)
                        {
                            View row =  super.getView(position, convertView, parent);
                            ImageView icon = (ImageView) row.findViewById(R.id.icon);
                            return row;
                        }
                    };
                //Llamo al adaptador
                    lstOpciones.setAdapter(myAdapter);

                //Genero el listener para detectar cuando presiono la lista
                    lstOpciones.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id)
                        {
                            //Genero variable para almacenar mi sharedpreference
                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                String[] args =new String[]{ list.get(position)};
                                db_cur = db.query("Usuarios", campos, "name=?", args, null, null, null);
                                db_cur.moveToFirst();

                                //Creo un intent y paso el elemento seleccionado
                                    Intent lst_act = new Intent(MainActivity.this, ListSelActivity.class);
                                    //Guardo el item seleccionado en las shared preferences
                                        editor.putString(Name, list.get(position));
                                        editor.commit();
                                    startActivity(lst_act);
                        }
                    });


        //Registro a la lista en el contexmenu
            registerForContextMenu(lstOpciones);

    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = getMenuInflater();

        if(v.getId() == R.id.LstOpciones)
        {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

            menu.setHeaderTitle("Opciones");

            inflater.inflate(R.menu.menu_ctx_lista, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        String[] args;
        AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo) item.getMenuInfo();


        switch (item.getItemId())
        {


            case R.id.erase:
                args =new String[]{ list.get(info.position)};
                db.delete("Usuarios", "name=?", args);
                Toast.makeText(MainActivity.this, list.get(info.position) + " fue borrado", Toast.LENGTH_LONG).show();
                list.remove(info.position);
                lstOpciones.setAdapter(myAdapter);
                if(list.isEmpty())
                {
                    //Lista vacia
                    list.add("dummy_Ferrari");
                    nuevoRegistro.put("value", 0);
                    nuevoRegistro.put("name","dummy_Ferrari");
                    nuevoRegistro.put("storage","Antartica");
                    db.insert("Usuarios", null, nuevoRegistro);

                }
                return true;
            case R.id.edit:
                    Intent edit_intent = new Intent(MainActivity.this, EditActivity.class);
                    args =new String[]{ list.get(info.position)};
                    db_cur = db.query("Usuarios", campos, "name=?", args, null, null, null);
                    db_cur.moveToFirst();
                    edit_intent.putExtra("action_flag",false);//id;value
                    edit_intent.putExtra("ferraris_value",db_cur.getInt(0));//id;value
                    edit_intent.putExtra("ferraris_name",list.get(info.position));
                    edit_intent.putExtra("ferraris_storage",db_cur.getString(2));
                    startActivityForResult(edit_intent,1);//intent_id;request_code
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }



//Aca recupero lo que devuelven los intents
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode)
        {
            case 1:
                if(resultCode == Activity.RESULT_OK)
                {
                    //Edito valores preexistentes

                    //Recorro la base de datos para llenar la lista con el valor modificado
                        db_cur = db.query("Usuarios", campos, null, null, null, null, null);
                        list.clear();
                        if (db_cur.moveToFirst())
                            {
                                //Recorremos el cursor hasta que no haya más registros
                                do {
                                    list.add(db_cur.getString(1));

                                } while (db_cur.moveToNext());
                            }
                    //Llamo al adaptador
                        lstOpciones.setAdapter(myAdapter);


                }

                if (resultCode == Activity.RESULT_CANCELED)
                {
                    //Write your code if there's no result
                }
                break;

            case 2:
                //Recibo el nuevo valor y lo almaceno


                if(resultCode == Activity.RESULT_OK)
                {
                   //Recibo los nuevos valores


                    //Recorro la base de datos para llenar la lista con el valor modificado
                        db_cur = db.query("Usuarios", campos, null, null, null, null, null);
                        list.clear();
                        if (db_cur.moveToFirst())
                        {
                            //Recorremos el cursor hasta que no haya más registros
                            do {
                                list.add(db_cur.getString(1));

                            } while (db_cur.moveToNext());
                        }
                        //Llamo al adaptador
                        lstOpciones.setAdapter(myAdapter);


                }

                if (resultCode == Activity.RESULT_CANCELED)
                {
                    //Write your code if there's no result
                }
                break;

            case 3:
                //Cambio el color del fondo

                if(resultCode == Activity.RESULT_OK)
                {
                    ConstraintLayout activity_main = (ConstraintLayout) findViewById(R.id.activity_main);
                    SharedPreferences prefs = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String bckgr_col_sel = prefs.getString(bckgr_col,"white");
                    switch (bckgr_col_sel)
                    {
                        case "white":
                            //Blanco
                            activity_main.setBackgroundColor(Color.WHITE);
                            pressed_Col = "white";
                            break;
                        case "green":
                            //Verde
                            activity_main.setBackgroundColor(Color.GREEN);
                            pressed_Col = "green";
                            break;
                        case "gray":
                            //Gris
                            activity_main.setBackgroundColor(Color.GRAY);
                            pressed_Col = "gray";
                            break;

                        case "blue":
                            //Azul
                            activity_main.setBackgroundColor(Color.BLUE);
                            pressed_Col = "blue";
                            break;
                    }
                }

                if (resultCode == Activity.RESULT_CANCELED)
                {
                    //Write your code if there's no result
                }
                break;

        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //Uso este método para modificar el color del fondo de pantalla
            ConstraintLayout activity_main = (ConstraintLayout) findViewById(R.id.activity_main);
                switch (bckgr_col)
                {
                    case "white":
                        activity_main.setBackgroundColor(Color.WHITE);
                        break;
                    case "green":
                        activity_main.setBackgroundColor(Color.GREEN);
                        break;
                    case "gray":
                        activity_main.setBackgroundColor(Color.GRAY);
                        break;
                    case "blue":
                        activity_main.setBackgroundColor(Color.BLUE);
                        break;
                }


    }

    //Borro la base de datos al finalizar el programa asi no acumula valores
    @Override
    public void onDestroy()
    {

        super.onDestroy();
        /*
         *Borro
         *   Tabla ( como borro la base puedo obviar este paso)
         *   Base de datos
         *   para que no se acumulen los valores y cada vez que cierro la app empieze desde 0, sin tener que
         *   actualizar la versión. ES IMPORTANTE RESPETAR EL ORDEN DE LAS INSTRUCCIONES EN ESTA
         *   SECUENCIA!!!!
         */
        //db.execSQL("DROP TABLE IF EXISTS Usuarios");
        this.deleteDatabase("DBUsuarios");
        db.close();

    }
}
