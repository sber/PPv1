package com.zerosa.ppv1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.SharedPreferences;




public class TabFragment2 extends Fragment
{
    public TextView tab2_txt;
    ContentValues valores= new ContentValues();
    ContentValues nuevoRegistro = new ContentValues();
    Cursor db_cur;
    String[] campos;


    //variables para llenar la db
    int value,i=0;
    String name,storage;

    //Abrimos la base de datos 'DBUsuarios' en modo escritura
    UsuariosSQLiteHelper usdbh;

    SQLiteDatabase db;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";

    public TabFragment2()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.tab_view2, container, false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void onActivityCreated(Bundle state)
    {
        super.onActivityCreated(state);
        tab2_txt = (TextView)getView().findViewById(R.id.tab2_txt);

        //Recupero el item seleccionado de las sharedpreferences
        SharedPreferences prefs = this.getActivity().getSharedPreferences(MyPREFERENCES, getContext().MODE_PRIVATE);
        String pressed_item = prefs.getString(Name,null);

        //Leo los valores de la base de datos
        usdbh = new UsuariosSQLiteHelper(getContext(), "DBUsuarios", null, 1);
        db = usdbh.getWritableDatabase();
        campos = new String[]{"value", "name", "storage"};
        String[] args =new String[]{ pressed_item};
        db_cur = db.query("Usuarios", campos, "name=?", args, null, null, null);
        db_cur.moveToFirst();

        //Inserto el valor en el fragment
        tab2_txt.setText(db_cur.getString(1));
    }

}