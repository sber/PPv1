package com.zerosa.ppv1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

public class UsuariosSQLiteHelper extends SQLiteOpenHelper
{

    //Sentencia SQL para crear la tabla de Usuarios
    String sqlCreate_prog = "create table if not exists Usuarios (value INTEGER, name TEXT,storage TEXT)";
    String sqlCreate_usrs = "create table if not exists app_usrs (usr TEXT,pass TEXT)";


    public UsuariosSQLiteHelper(Context contexto, String name,
                                CursorFactory factory, int version)
    {
        super(contexto, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        //Se ejecuta la sentencia SQL de creación de la tabla
        db.execSQL(sqlCreate_prog);
        db.execSQL(sqlCreate_usrs);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva)
    {
        //NOTA: Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        //      eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.
        //      Sin embargo lo normal será que haya que migrar datos de la tabla antigua
        //      a la nueva, por lo que este método debería ser más elaborado.

        //Se elimina la versión anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS Usuarios");
        db.execSQL("DROP TABLE IF EXISTS app_usrs");

        //Se crea la nueva versión de la tabla
        db.execSQL(sqlCreate_prog);
        db.execSQL(sqlCreate_usrs);
    }

}