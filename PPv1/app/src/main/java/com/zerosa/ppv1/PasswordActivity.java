package com.zerosa.ppv1;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordActivity extends Activity

{
    public Button bttn_chng_psw;
    public EditText txt_new_psw;

    //variables para llenar la db
        UsuariosSQLiteHelper usdbh;
        SQLiteDatabase db;
        ContentValues valores= new ContentValues();
        Cursor db_cur;
        String[] campos;
        String[] args;


    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String sel_usr = "sel_usr";

    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.psw_view);
        txt_new_psw = (EditText)findViewById(R.id.txt_new_psw);
        bttn_chng_psw = (Button)findViewById(R.id.bttn_chng_psw);

        //Recupero el usuario empleado
            SharedPreferences prefs = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String usr_2_modif = prefs.getString(sel_usr,"usuario1");

        //Abro la base de datos para cargar la nueva contraseña
            usdbh = new UsuariosSQLiteHelper(this, "DBusrs", null, 1);
            db = usdbh.getWritableDatabase();
            campos = new String[]{"usr", "pass"};
            args =new String[]{ usr_2_modif};


        bttn_chng_psw.setOnClickListener(new View.OnClickListener()
        {


            @Override
            public void onClick(View v)
            {

                //Solo modifico el valor si se ingresó algo
                    if (!txt_new_psw.getText().toString().equals(""))
                    {

                        //Leo el valor ingresado
                            valores.put("pass", txt_new_psw.getText().toString());
                        //Actualizamos el registro en la base de datos
                            db.update("app_usrs", valores, "usr=?", args);
                        //Utilizo el cursor para indicar el usuario del cual se cambia la clave
                        db_cur = db.query("app_usrs", campos, "usr=?", args, null, null, null);
                        db_cur.moveToFirst();

                        Toast.makeText(getApplicationContext(),
                                "La contraseña del "+db_cur.getString(0)+" fue cambiada",Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                        //Utilizo el cursor para indicar el usuario del cual se cambia la clave
                        db_cur = db.query("app_usrs", campos, "usr=?", args, null, null, null);
                        db_cur.moveToFirst();

                        Toast.makeText(getApplicationContext(),
                                "La contraseña del "+db_cur.getString(0)+" no fue cambiada",Toast.LENGTH_SHORT).show();
                        finish();
                    }



            }});
    }
}
