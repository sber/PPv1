package com.zerosa.spv2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

//Imports FireBase
    import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

//Imports SharedPreferences


public  class  FireBaseAdapter extends FirebaseRecyclerAdapter<recViewElements, recViewHolder>
{

    private Context context;
    public int position;
    public String childKey;
    DatabaseReference  dbFerraris,dbSelChild; //La declaro global para que no me pida hacerla final dentro de los listeners
    String del_Ferrari;

    //Gestion de sharedpreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String shChildKey = "childkey";
        public static final String last_pressed_back_col = "prssd_col";
        public static final String bckgr_col = "col_key";

    //Variable para checkean el boton correspondiente en settings
        String pressed_Col="white";//valor por default blanco

    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public FireBaseAdapter(Class<recViewElements> modelClass, int modelLayout, Class<recViewHolder> viewHolderClass, DatabaseReference dbref, Context context)
    {
        super(modelClass, modelLayout, viewHolderClass, dbref);
        this.context = context;
    }

    @Override
    protected void populateViewHolder(recViewHolder recViewHld, recViewElements recViewElem, final int position)
    {
        recViewHld.bindrevViewElements(recViewElem);
        recViewHld.txtModel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {
                setPosition(position);
                return false;
            }

        });

        recViewHld.bttn_edit_item.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

/*                Toast.makeText(v.getContext(), "Rec_view bttn "+ position +" pressed!!",
                        Toast.LENGTH_LONG).show();
*/
                //Guardo la clave del elemento seleccionado
                    String aux = getRef(position).toString();
                    childKey = aux.substring(aux.lastIndexOf('/') + 1);
                    //childKey = ((getRef(position)).toString()).substring(50); //old way
                //Genero variable para almacenar mis sharedpreference
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                //Guardo el item seleccionado en las shared preferences
                    editor.putString(shChildKey, childKey);
                    editor.commit();

                //Voy a la activity de edición
                    Intent edit_intent = new Intent(v.getContext(), EditActivity.class);
                    edit_intent.putExtra("action_flag",false);//id;value
                    ((Activity) context).startActivity(edit_intent);//intent_id;
            }
        });
    }

    @Override
    public recViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        //Muy importante poner context.getSharedPreferences(...)
            sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);




        recViewHolder viewHolder = super.onCreateViewHolder(parent, viewType);




        viewHolder.setOnClickListener(new recViewHolder.ClickListener()
        {
            @Override
            public void onItemClick(View view, int position)
            {
                /*
                *   This section is used to determine the selected item in the
                *   recyclerview and proceed to the fragments.
                 */
/*
                Toast.makeText(view.getContext(), "Item clicked at " + position, Toast.LENGTH_SHORT).show();
*/
                //Guardo la clave del elemento seleccionado
                        childKey = ((getRef(position)).toString()).substring(50);
                    //Genero variable para almacenar mis sharedpreference
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                    //Guardo el item seleccionado en las shared preferences
                        editor.putString(shChildKey, childKey);
                        editor.commit();

                Intent lst_act = new Intent(view.getContext(), ListSelActivity.class);
                ((Activity) context).startActivity(lst_act);


            }
            @Override
            public void onItemLongClick(View view, int position)
            {

                Toast.makeText(view.getContext(), "La Ferrari fue borrada", Toast.LENGTH_SHORT).show();
                //Borro el elemento selecionado
                    getRef(position).removeValue();
                    setPosition(position);
            }


        });
        return viewHolder;
    }

}