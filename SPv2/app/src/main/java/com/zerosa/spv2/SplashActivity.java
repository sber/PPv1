package com.zerosa.spv2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity
{

    private ProgressBar pbarProgreso;
    private ProgressDialog pDialog;
    private MiTareaAsincronaDialog tarea2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        pDialog = new ProgressDialog(SplashActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);//STYLE_HORIZONTAL;STYLE_SPINNER
        pDialog.setMessage("Procesando...");
        pDialog.setCancelable(false);//No permito que sea interrumpido al tocar la pantalla
        pDialog.setMax(100);

        tarea2 = new MiTareaAsincronaDialog();
        tarea2.execute();
    }

    private void tareaLarga()
    {
        try
        {
            //Sleep de 1/2''
                Thread.sleep(500);
        }
        catch(InterruptedException e)
        {

        }
    }


    private class MiTareaAsincronaDialog extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            for(int i=1; i<=10; i++) {
                tareaLarga();

                publishProgress(i*10);

                if(isCancelled())
                    break;
            }

            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

            pDialog.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {

            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    MiTareaAsincronaDialog.this.cancel(true);
                }
            });

            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
            {
                pDialog.dismiss();
                //Toast.makeText(SplashActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
                Intent log_act=new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(log_act);

                //Destruyo la activity asi no se puede volver
                    finish();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(SplashActivity.this, "Tarea cancelada!", Toast.LENGTH_SHORT).show();
        }
    }
}
