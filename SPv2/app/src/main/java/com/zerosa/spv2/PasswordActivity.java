package com.zerosa.spv2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

//Imports para FireBase
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class PasswordActivity extends AppCompatActivity

{

    public ImageButton bttn_add,bttn_sett,bttn_edit_psw;
    public EditText txt_new_psw;

    //Herramientas SharedPreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String sel_usr = "sel_usr";
        public static final String sel_usr_pwd = "sel_usr_pwd";

        String newPass ="123abc";

    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.psw_view);

        txt_new_psw = (EditText)findViewById(R.id.txt_new_psw);

        //Escondo los botones del Toolbar que no deseo ver
        //Por algun motivo que desconozco no se ven los imgbttn en esta activity
        //Tenia que hacer que la activity extienda AppCompatActivity
            bttn_add = (ImageButton) findViewById(R.id.bttn_add);
            bttn_add.setVisibility(View.VISIBLE);
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setVisibility(View.GONE);
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
            bttn_edit_psw.setVisibility(View.VISIBLE);

        //Para poder cambiar la contraseña FireBase requiere saber el usuario y la contraseña actual
            //Recupero el usuario empleado
                SharedPreferences prefs = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                     String usr_2_modif = prefs.getString(sel_usr,"usuario1");
                     String old_pwd = prefs.getString(sel_usr_pwd,"110022");

            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final AuthCredential credential = EmailAuthProvider.getCredential(usr_2_modif, old_pwd);

        bttn_edit_psw.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                newPass = txt_new_psw.getText().toString();
                //Solo modifico el valor si se ingresó algo
                    if (!newPass.equals(""))
                    {
                        //FireBase pide que la clave sea de 6 o más caracteres
                            if(newPass.length()>5)
                            {
                                //Se cambió la clave
                                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        if (task.isSuccessful())
                                        {
                                            user.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>()
                                            {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task)
                                                {
                                                    if (task.isSuccessful())
                                                        Toast.makeText(getApplicationContext(),
                                                                "La clave fue cambiada",Toast.LENGTH_SHORT).show();
                                                    else
                                                        Toast.makeText(getApplicationContext(),
                                                                "Ocurrió un error, la clave no puedo ser modificada, intente más tarde" +
                                                                        "",Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            else
                                Toast.makeText(getApplicationContext(),
                                        "Ingrese una clave de 6 o más caracteres",Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        //El campo clave está vacio
                        Toast.makeText(getApplicationContext(),"No se ingresó una nueva clave",Toast.LENGTH_SHORT).show();
                    }
            }});

        bttn_add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
                startActivity(new Intent(PasswordActivity.this, FireBaseAuthActivity.class));
            }
        });
    }
}
