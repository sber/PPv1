package com.zerosa.spv2;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.SharedPreferences;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class TabFragment3 extends Fragment
{
    public TextView tab3_txt;



    //Herramientas de FireBase
        DatabaseReference dbFragElem;

    //Herramientas de SharedPreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String shChildKey = "childkey";

    //Variable para gestionar la info a mostrar en el fragment
        String str_old_loc="Hogwarts",str_old_loc1 = "Haveen";

    public TabFragment3()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.tab_view3, container, false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void onActivityCreated(Bundle state)
    {
        super.onActivityCreated(state);
        tab3_txt = (TextView)getView().findViewById(R.id.tab3_txt);

        //Recupero el item seleccionado de las sharedpreferences
            SharedPreferences prefs = this.getActivity().getSharedPreferences(MyPREFERENCES, getContext().MODE_PRIVATE);
            final String childKey = prefs.getString(shChildKey,null);

        //Abro la DB FireBase
        dbFragElem =FirebaseDatabase.getInstance().getReference().child("recViewElements").child(childKey);

        //Genero al listener para acceder a los datos y actualizar en caso de cambios
        dbFragElem.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                //Recupero el precio
                recViewElements elem = dataSnapshot.getValue(recViewElements.class);
                    str_old_loc=elem.getLocation();

                //Alternativa 2
                //str_old_loc =dataSnapshot.child("location").getValue().toString();

                //Inserto el valor en el fragment
                    tab3_txt.setText(str_old_loc);// Si no lo pongo aca no lo lee, dunno why
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //Sin acción
            }
        });
    }

}