package com.zerosa.spv2;
import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

//Import SharedPreferences
    import android.content.SharedPreferences;

//Imports para FireBase
import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity
{
    //Gestion de sharedpreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String SharedName = "nameKey";
        public static final String last_pressed_back_col = "prssd_col";
        public static final String bckgr_col = "col_key";
        public static final String shChildKey = "childkey";

    //Variable para checkean el boton correspondiente en settings
        String pressed_Col="white";//valor por default blanco

    //Botones del toolbar
        public ImageButton bttn_add,bttn_sett,bttn_edit_psw;

    //Herramientas del RecyclerView
        private RecyclerView recView;

    //Herramientas FireBase
        private static final String TAGLOG = "firebase-db";
        FireBaseAdapter mAdapter;
        //FirebaseRecyclerAdapter mAdapter;
        DatabaseReference dbFerraris;
        String childKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        //Configuro el toolbar y sus botones

        //Asigno el toolbar a la activity
            Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
            setSupportActionBar(toolbar);

        //Configuro el botton para agregar
            bttn_add = (ImageButton) findViewById(R.id.bttn_add);
            bttn_add.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Intent edit_intent = new Intent(MainActivity.this, EditActivity.class);
                    edit_intent.putExtra("action_flag", true);//id;value
                    startActivityForResult(edit_intent, 2);//intent_id;request_code
                }

            });


        //Configuro el botton para ir a las configuraciones
            //java:SettingsActivity>PrefsFragment
            //xml:res>xml
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SharedPreferences.Editor settings = sharedpreferences.edit();
                    //Guardo el item seleccionado en las shared preferences
                    settings.putString(last_pressed_back_col, pressed_Col);
                    settings.commit();
                    Intent sett_intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivityForResult(sett_intent, 1);//intent_id;request_code
                }

            });

        //Configuro el botton para ir al cambio de contraseña
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
            bttn_edit_psw.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Intent psw_intent = new Intent(MainActivity.this, PasswordActivity.class);
                    startActivity(psw_intent);//intent_id;
                }

            });

        //Accedo a instancia de DB de FireBase
            dbFerraris =FirebaseDatabase.getInstance().getReference()
                        .child("recViewElements");

        //Manejo del Recyclerview
            //Declaro instancia
                RecyclerView RecView = (RecyclerView) findViewById(R.id.RecView);
            //Configuro la parte estética
                RecView.setHasFixedSize(true);
                RecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                RecView.addItemDecoration(
                        new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
                RecView.setItemAnimator(new DefaultItemAnimator());
            //Configuro al adaptador del Recycleview de FireBase
                mAdapter = new FireBaseAdapter(recViewElements.class, R.layout.recview_items, recViewHolder.class, dbFerraris,this);
            //Llamo al adaptador
                RecView.setAdapter(mAdapter);
    }

//Aca recupero lo que devuelven los intents
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode)
        {
            case 1:
                //Cambio el color del fondo

                if(resultCode == Activity.RESULT_OK)
                {
                    ConstraintLayout activity_main = (ConstraintLayout) findViewById(R.id.activity_main);
                    SharedPreferences prefs = this.getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                    String bckgr_col_sel = prefs.getString(bckgr_col,"white");
                    switch (bckgr_col_sel)
                    {
                        case "white":
                            //Blanco
                            activity_main.setBackgroundColor(Color.WHITE);
                            pressed_Col = "white";
                            break;
                        case "green":
                            //Verde
                            activity_main.setBackgroundColor(Color.GREEN);
                            pressed_Col = "green";
                            break;
                        case "gray":
                            //Gris
                            activity_main.setBackgroundColor(Color.GRAY);
                            pressed_Col = "gray";
                            break;

                        case "blue":
                            //Azul
                            activity_main.setBackgroundColor(Color.BLUE);
                            pressed_Col = "blue";
                            break;
                    }
                }

                if (resultCode == Activity.RESULT_CANCELED)
                {
                    //Write your code if there's no result
                }
                break;

        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //Uso este método para modificar el color del fondo de pantalla
            ConstraintLayout activity_main = (ConstraintLayout) findViewById(R.id.activity_main);
                switch (bckgr_col)
                {
                    case "white":
                        activity_main.setBackgroundColor(Color.WHITE);
                        break;
                    case "green":
                        activity_main.setBackgroundColor(Color.GREEN);
                        break;
                    case "gray":
                        activity_main.setBackgroundColor(Color.GRAY);
                        break;
                    case "blue":
                        activity_main.setBackgroundColor(Color.BLUE);
                        break;
                }


    }

    //Libero al adaptador
    @Override
    public void onDestroy()
    {

        super.onDestroy();
        mAdapter.cleanup();

    }
}
