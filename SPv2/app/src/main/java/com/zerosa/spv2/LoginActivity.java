package com.zerosa.spv2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import android.content.SharedPreferences;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    //defining views
        private ImageButton buttonSignIn;
        private EditText editTextEmail;
        private EditText editTextPassword;
//        private TextView textViewSignup;

    //firebase auth object
    private FirebaseAuth firebaseAuth;

    //progress dialog
        private ProgressDialog progressDialog;

    //Herramientas SharedPreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String sel_usr = "sel_usr";
        public static final String sel_usr_pwd = "sel_usr_pwd";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        //getting firebase auth object
            firebaseAuth = FirebaseAuth.getInstance();
/*
        //Comento esta sección para que siempre pida las credenciales
        //if the objects getcurrentuser method is not null
        //means user is already logged in

        if(firebaseAuth.getCurrentUser() != null)
        {
            //close this activity
            finish();
            //opening profile activity
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
*/
        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonSignIn = (ImageButton) findViewById(R.id.buttonSignin);
//        textViewSignup  = (TextView) findViewById(R.id.textViewSignUp);

        progressDialog = new ProgressDialog(this);

        //attaching click listener
        buttonSignIn.setOnClickListener(this);
//        textViewSignup.setOnClickListener(this);
    }

    //method for user login
    private void userLogin(){
        final String email = editTextEmail.getText().toString().trim();
        final String password  = editTextPassword.getText().toString().trim();


        //checking if email and passwords are empty
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Porfavor ingrese su email",Toast.LENGTH_LONG).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            Toast.makeText(this,"Porfavor ingrese su clave",Toast.LENGTH_LONG).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Corroborando datos...");
        progressDialog.show();

        //logging in the user
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        progressDialog.dismiss();
                        //if the task is successfull
                        if(task.isSuccessful())
                        {
                            //Ingresé un usuario válido
                                //Guardo el mail ingresado, para poder cambiar la pswd
                                    SharedPreferences.Editor settings = sharedpreferences.edit();
                                    settings.putString(sel_usr, email);
                                    settings.putString(sel_usr_pwd, password);
                                    settings.commit();
                                //Borro la actividad asi no la vuelvo a ver
                                    finish();
                                //Presento mensaje de bienvenida
                                    Toast.makeText(getApplicationContext(),"Bienvenido a la app",Toast.LENGTH_SHORT).show();
                                //Voy a la pantalla principal
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));

                        }
                        progressDialog.setMessage("Cuenta o clave incorrecta, intente nuevamente...");

                    }
                });

    }

    @Override
    public void onClick(View view)
    {
        if(view == buttonSignIn)
        {
            userLogin();
        }
/*
        if(view == textViewSignup)
        {
            //Se quiere ir a la pantalla de creación de usuarios
                finish();
                startActivity(new Intent(this, FireBaseAuthActivity.class));
        }
*/
    }
}



