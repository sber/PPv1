package com.zerosa.spv2;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.SharedPreferences;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zerosa.spv2.R;


public class TabFragment1 extends Fragment
{
    public TextView tab1_txt;

    //Herramientas de FireBase
        DatabaseReference dbFragElem;

    //Herramientas de SharedPreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String shChildKey = "childkey";

    //Variable para gestionar la info a mostrar en el fragment
        String str_old_price="0,00",str_old_price1 = "1,11";

    //Herramientas del RecyclerView
      private RecyclerView recView;

    //Herramientas FireBase
    private static final String TAGLOG = "firebase-db";
    FireBaseAdapter mAdapter;
    //FirebaseRecyclerAdapter mAdapter;
    DatabaseReference dbFerraris;
    String childKey;
        
    public TabFragment1()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {



        return inflater.inflate(R.layout.tab_view1, container, false);

//  With this code i can open the main list in a fragment, it is now being used

//        View rootView = inflater.inflate(R.layout.activity_main, container, false);
//
//        //Accedo a instancia de DB de FireBase
//            dbFerraris =FirebaseDatabase.getInstance().getReference()
//                .child("recViewElements");
//
//        //Manejo del Recyclerview
//            //Declaro instancia
//                RecyclerView RecView = (RecyclerView) rootView.findViewById(R.id.RecView);
//
//            //Configuro la parte estética
//                RecView.setHasFixedSize(true);
//                RecView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
//                RecView.addItemDecoration(
//                        new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
//                RecView.setItemAnimator(new DefaultItemAnimator());
//
//            //Configuro al adaptador del Recycleview de FireBase
//                mAdapter = new FireBaseAdapter(recViewElements.class, R.layout.recview_items, recViewHolder.class, dbFerraris,getActivity());
//
//            //Llamo al adaptador
//                RecView.setAdapter(mAdapter);
//        return rootView;

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void onActivityCreated(Bundle state)
    {
       super.onActivityCreated(state);
        tab1_txt = (TextView)getView().findViewById(R.id.tab1_txt);

        //Recupero el item seleccionado de las sharedpreferences
           SharedPreferences prefs = this.getActivity().getSharedPreferences(MyPREFERENCES, getContext().MODE_PRIVATE);
           final String childKey = prefs.getString(shChildKey,null);

        //Abro la DB FireBase
            dbFragElem =FirebaseDatabase.getInstance().getReference().child("recViewElements").child(childKey);

        //Genero al listener para acceder a los datos y actualizar en caso de cambios
            dbFragElem.addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    //Recupero el precio
                        recViewElements elem = dataSnapshot.getValue(recViewElements.class);
                        str_old_price="$"+elem.getPrice();

                        //Alternativa 2
                            //str_old_price1 =dataSnapshot.child("price").getValue().toString();

                   //Inserto el valor en el fragment
                        tab1_txt.setText(str_old_price);// Si no lo pongo aca no lo lee, dunno why
                }
                @Override
                public void onCancelled(DatabaseError databaseError)
                {
                    //Sin acción
                }
            });
            
    }

}