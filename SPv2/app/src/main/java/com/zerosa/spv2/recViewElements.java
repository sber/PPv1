package com.zerosa.spv2;

public class recViewElements {
        private String location;
        private String model;
        private String price;

        public recViewElements()
        {
            //Es obligatorio incluir constructor por defecto
        }

        public recViewElements(String location, String model, String price)
        {
            this.location = location;
            this.model = model;
            this.price = price;

        }

        public String getModel()
        {
            return model;
        }

        public void setModel(String model)
        {
            this.model = model;
        }

        public String getLocation() {
                return location;
        }

        public void setLocation(String location) {
                this.location = location;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

    @Override
        public String toString()
        {
            return "recViewElements{" +
                    ", modelo='" + model  + '}';

        }
}