package com.zerosa.spv2;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FireBaseAuthActivity extends AppCompatActivity implements View.OnClickListener
{
    //defining view objects
        private EditText editTextEmail;
        private EditText editTextPassword;
        private Button buttonSignup;

    public ImageButton bttn_add,bttn_sett,bttn_edit_psw;

//    private TextView textViewSignin;

    private ProgressDialog progressDialog;

    //Pongo esto para que TAG no arroje error
        private static final String TAG = "FireBaseAuthActivity";

    //defining firebaseauth object
        private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firebaseauth_view);        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();



        //Escondo los botones del Toolbar que no deseo ver
        bttn_add = (ImageButton) findViewById(R.id.bttn_add);
        bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
        bttn_sett.setVisibility(View.GONE);
        bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
        bttn_edit_psw.setVisibility(View.GONE);
/*
         //Comento esta sección para que siempre pida las credenciales
        //if getCurrentUser does not returns null
        if(firebaseAuth.getCurrentUser() != null){
            //that means user is already logged in
            //so close this activity
            finish();

            //and open profile activity
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
*/
        //initializing views
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
//        textViewSignin = (TextView) findViewById(R.id.textViewSignin);

//        buttonSignup = (Button) findViewById(R.id.buttonSignup);

        progressDialog = new ProgressDialog(this);

        //attaching listener to button
        bttn_add.setOnClickListener(this);
//        textViewSignin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {

        if(view == bttn_add)
        {
            //Llamo a la función que registra al usuario
                registerUser();
            //Envio mail  de verificacion
                sendEmailVerification();
        }

    }

    private void registerUser()
    {

        //Obtengo mail y clave de los campos
            String email = editTextEmail.getText().toString().trim();
            String password  = editTextPassword.getText().toString().trim();

        //Verifico que se hayan introducido valores en ambos campos
            if(TextUtils.isEmpty(email))
            {
                Toast.makeText(this,"Ingrese un correo valido",Toast.LENGTH_LONG).show();
                return;
            }

            if(TextUtils.isEmpty(password))
            {
                Toast.makeText(this,"Ingrese una clave de 6 o más caracteres",Toast.LENGTH_LONG).show();
                return;
            }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Registrando la nueva cuenta...");
        progressDialog.show();

        //Creo nuevo usuario
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task)
                        {
                            if(task.isSuccessful())
                                //La registración fue exitosa
                                    Toast.makeText(FireBaseAuthActivity.this,"El usuario fue creado",Toast.LENGTH_LONG).show();
                            else
                                //Ocurrió un error y no fue posible la registración
                                    Toast.makeText(FireBaseAuthActivity.this,"Error en la registración , intente nuevamente",Toast.LENGTH_LONG).show();

                            progressDialog.dismiss();
                        }
                    });

    }

    private void sendEmailVerification()
    {

        final FirebaseUser user = firebaseAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Verification email sent to " + user.getEmail(), Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification failed!", task.getException());
                            Toast.makeText(getApplicationContext(), "Failed to send verification email.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


}