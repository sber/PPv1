package com.zerosa.spv2;

import android.app.Activity;
import android.os.Bundle;


//Esta Activity es la que contiene al fragment, que a su vez va a contener las settings
//java:SettingsActivity>PrefsFragment
//xml:res>xml
public class SettingsActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();
    }

}