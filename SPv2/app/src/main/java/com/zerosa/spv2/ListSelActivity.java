package com.zerosa.spv2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

public class ListSelActivity extends AppCompatActivity
{
    public TextView list_view_txt,tab1_txt,tab2_txt,tab3_txt;

    //Botontes del Toolbar
        public ImageButton bttn_add,bttn_sett,bttn_edit_psw;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        String pressed_item;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        list_view_txt = (TextView) findViewById(R.id.list_view_txt);
        tab1_txt = (TextView) findViewById(R.id.tab1_txt);
        tab2_txt = (TextView) findViewById(R.id.tab2_txt);
        tab3_txt = (TextView) findViewById(R.id.tab3_txt);

        //Escondo los botones del Toolbar que no deseo ver
            bttn_add = (ImageButton) findViewById(R.id.bttn_add);
            bttn_add.setVisibility(View.GONE);
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setVisibility(View.GONE);
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
            bttn_edit_psw.setVisibility(View.GONE);


        //Bundle extras = getIntent().getExtras();
        //pressed_item= extras.getString("pressed_item");

        //list_view_txt.setText(pressed_item);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Precio"));
        //tabLayout.addTab(tabLayout.newTab().setText("Foto"));
        tabLayout.addTab(tabLayout.newTab().setText("Ubicacion"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            //Bundle extras = getIntent().getExtras();

            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                //extras.getInt("ferraris_value");
                viewPager.setCurrentItem(tab.getPosition());


                switch (tab.getPosition())
                {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });

        /*
        *Aca va el codigo para ir a las settings desde este activity
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {

                    Intent sett_intent = new Intent(ListSelActivity.this, SettingsActivity.class);
                    startActivity(sett_intent);//intent_id;request_code

                }

            });
        */

    }
}
