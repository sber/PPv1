package com.zerosa.spv2;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.SharedPreferences;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class TabFragment2 extends Fragment
{
    public TextView tab2_txt;

    //Herramientas de FireBase
        DatabaseReference dbFragElem;

    //Herramientas de SharedPreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String shChildKey = "childkey";

    //Variable para gestionar la info a mostrar en el fragment
        String str_old_price="0,00",str_old_price1 = "1,11";

    public TabFragment2()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.tab_view2, container, false);

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }
    @Override
    public void onActivityCreated(Bundle state)
    {
        super.onActivityCreated(state);
        // NO ESTOY MOSTRANDO ESTE FRAGMENT!!!


        tab2_txt = (TextView)getView().findViewById(R.id.tab2_txt);

        //Recupero el item seleccionado de las sharedpreferences
            SharedPreferences prefs = this.getActivity().getSharedPreferences(MyPREFERENCES, getContext().MODE_PRIVATE);
            final String childKey = prefs.getString(shChildKey,null);

        //Abro la DB FireBase
            dbFragElem =FirebaseDatabase.getInstance().getReference().child("recViewElements").child(childKey);

        //Genero al listener para acceder a los datos y actualizar en caso de cambios
            dbFragElem.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                //Recupero el precio
                    recViewElements elem = dataSnapshot.getValue(recViewElements.class);
                    str_old_price=elem.getPrice();

                //Alternativa 2
                //str_old_price1 =dataSnapshot.child("price").getValue().toString();

                //Inserto el valor en el fragment
                    tab2_txt.setText(str_old_price);// Si no lo pongo aca no lo lee, dunno why
            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //Sin acción
            }
        });
    }

}