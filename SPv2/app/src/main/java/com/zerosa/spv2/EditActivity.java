package com.zerosa.spv2;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

//Imports para FireBase
import com.google.firebase.database.DataSnapshot;
    import com.google.firebase.database.DatabaseError;
    import com.google.firebase.database.DatabaseReference;
    import com.google.firebase.database.FirebaseDatabase;
    import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class EditActivity extends AppCompatActivity
{

    public ImageButton bttn_add,bttn_sett,bttn_edit_psw;
    public EditText txt_loc_edit,txt_price_edit,txt_name_edit;
    String pressed_item,new_loc,new_name,new_price;
    Bundle extras;

    //Herramientas FireBase
        private static final String TAGLOG = "firebase-db";
        private ValueEventListener eventListener,childEventListener;
        DatabaseReference  dbFerraris,dbSelChild; //La declaro global para que no me pida hacerla final dentro de los listeners

    //Gestion de sharedpreferences
        SharedPreferences sharedpreferences;
        public static final String MyPREFERENCES = "MyPrefs" ;
        public static final String shChildKey = "childkey";

    //Variables para gestionar la edición de elementos
        String str_old_price = "0,00",str_old_loc = "neverland";
        Map<String, Object> updated_values = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        extras = getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_view);
        txt_price_edit = (EditText) findViewById(R.id.txt_price_edit);
        txt_loc_edit = (EditText) findViewById(R.id.txt_loc_edit);
        txt_name_edit = (EditText) findViewById(R.id.txt_name_edit);


        //Escondo los botones del Toolbar que no deseo ver
            bttn_add = (ImageButton) findViewById(R.id.bttn_add);
            bttn_add.setVisibility(View.GONE);
            bttn_sett = (ImageButton) findViewById(R.id.bttn_sett);
            bttn_sett.setVisibility(View.GONE);
            bttn_edit_psw = (ImageButton) findViewById(R.id.bttn_edit_psw);
            bttn_edit_psw.setVisibility(View.GONE);



        //Abro la DB FireBase
            dbFerraris =FirebaseDatabase.getInstance().getReference().child("recViewElements");


        if (extras.getBoolean("action_flag"))
        {
            /*Agrego un item*/

            bttn_edit_psw.setVisibility(View.GONE);
            bttn_sett.setVisibility(View.GONE);
            bttn_add.setVisibility(View.VISIBLE);
            txt_name_edit.setVisibility(View.VISIBLE);


            bttn_add.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    //Genero mensaje si no se completó un campo
                    if (txt_price_edit.getText().toString().equals("")||txt_name_edit.getText().toString().equals("")||txt_loc_edit.getText().toString().equals(""))
                        Toast.makeText(getApplicationContext(),"La Ferrari no fue agregada, porfavor complete todos los campos",Toast.LENGTH_SHORT).show();

                    else
                    {

                        //Leo los valores ingresados
                            new_price = txt_price_edit.getText().toString();
                            new_name =txt_name_edit.getText().toString();
                            new_loc =txt_loc_edit.getText().toString();

                        //Inserto los valores en la tabla
                            recViewElements new_element = new recViewElements(new_loc,new_name,new_price);
                            dbFerraris.push().setValue(new_element);
                    }
                    //Retorno al main activity
                        finish();
                }
            });
        }

        else if(!extras.getBoolean("action_flag"))
        {
            /*Edito un item*/

            bttn_add.setVisibility(View.GONE);
            bttn_sett.setVisibility(View.GONE);
            bttn_edit_psw.setVisibility(View.VISIBLE);
            txt_name_edit.setVisibility(View.GONE);

            //Recupero la key de la Firebase
                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                final String childKey = sharedpreferences.getString(shChildKey,null);

            //Me posiciono en la DB donde está el child a editar
                dbSelChild=dbFerraris.child(childKey);
                dbSelChild.addListenerForSingleValueEvent(new ValueEventListener(){
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    str_old_price =dataSnapshot.child("price").getValue().toString();
                    str_old_loc = dataSnapshot.child("location").getValue().toString();

                }
                    @Override
                    public void onCancelled(DatabaseError databaseError) 
                    {
                        //Sin acción
                    }
                });


            bttn_edit_psw.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    //Verifico si se ingresan datos en caso que no se ingresen dejo el valor anterior
                    if(txt_price_edit.getText().toString().equals(""))
                        new_price=str_old_price;
                    else
                        new_price = txt_price_edit.getText().toString();

                    if(txt_loc_edit.getText().toString().equals(""))
                        new_loc=str_old_loc;
                    else
                        new_loc = txt_loc_edit.getText().toString();

                    //Actualizo el registro en la base de datos
                        updated_values.put("/location", new_loc);
                        updated_values.put("/price", new_price);
                        dbSelChild.updateChildren(updated_values);

                    finish();
                }

            });

        }

    }
}

