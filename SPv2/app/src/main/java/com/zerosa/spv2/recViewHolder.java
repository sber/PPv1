package com.zerosa.spv2;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;



/*
* La magia de un ViewHolder es que guarda las referencias de los objetos(txtViews,Botones,ImgViews,etc)
 * de los xmls que usamos, con el fin de no tener que buscarlos cada vez que haya que mostrar algo */

public  class recViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener
{
    private View mView;
    public TextView txtLocation,txtModel,txtPrice;
    private recViewHolder.ClickListener mClickListener;
    private int position;
    public ImageButton bttn_edit_item;

    public recViewHolder(View itemView)
    {
        super(itemView);
        mView = itemView;

        /*
         * En este constructor guardo las referencias de los elementos xml que voy a  usar
         */

        txtLocation = (TextView) mView.findViewById(R.id.txtLocation);
        txtModel = (TextView) mView.findViewById(R.id.txtModel);
        txtPrice = (TextView) mView.findViewById(R.id.txtPrice);
        bttn_edit_item = (ImageButton) mView.findViewById(R.id.bttn_edit_item);

        itemView.setOnCreateContextMenuListener(this);

        //listener set on ENTIRE ROW, you may set on individual components within a row.
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onItemClick(v, getAdapterPosition());
                /*Intent edit_intent = new Intent(v.getContext(), EditActivity.class);
                edit_intent.putExtra("action_flag", false);//id;value
                v.getContext().startActivity(edit_intent);//intent_id;*/
            }

        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mClickListener.onItemLongClick(v, getAdapterPosition());
                return true;
            }
        });



    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("Opciones");
        menu.add(0, 1, 0, "Editar");//groupId, itemId, order, title
        menu.add(0, 2, 0, "Borrar");


    }

    //Interface to send callbacks...
    public interface ClickListener
    {
        public void onItemClick(View view, int position);
        public void onItemLongClick(View view, int position);
    }

    public void setOnClickListener(recViewHolder.ClickListener clickListener)
    {
        mClickListener = clickListener;
    }

    public void bindrevViewElements(recViewElements elem)
    {

        /*
         * Esta función está para vincular los elementos del xml(, txtView, boton, imgview, etc)
         * con valores o acciones que quiera, i.e. aca es donde escribo en los txtviews el
         * valor que quiero que muestren
         */

        //txtLocation.setText(elem.getLocation());
        txtModel.setText(elem.getModel()); //Solo quiero mostrar el modelo en la pantalla principal
        /*String price = "$" + elem.getPrice();//Java recomienda hacer las concatenaciones en variables
        txtPrice.setText(price);*/
    }


}