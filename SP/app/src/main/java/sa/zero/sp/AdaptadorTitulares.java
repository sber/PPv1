package sa.zero.sp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdaptadorTitulares extends RecyclerView.Adapter<AdaptadorTitulares.TitularesViewHolder>
        implements View.OnClickListener
{

    private View.OnClickListener listener;
    private ArrayList<Titular> datos;

    public static class TitularesViewHolder extends RecyclerView.ViewHolder
    {
        //Creo el viewholder
        private TextView txtTitulo;
        private TextView txtSubtitulo;
        private ImageView imagen;
        private Button button;

        public TitularesViewHolder(View itemView)
        {
            super(itemView);

            //Indica a de donde van a recibir su valor los 2 TextViews
            txtTitulo = (TextView)itemView.findViewById(R.id.LblTitulo);
            txtSubtitulo = (TextView)itemView.findViewById(R.id.LblSubTitulo);
            imagen = (ImageView) itemView.findViewById(R.id.imageView2);
            button = (Button)itemView.findViewById(R.id.button);
        }

        public void bindTitular(Titular t)
        {
            //Llama al método de la clase Titular para asignar el valor
            //asigna los contenidos de los dos cuadros de texto a partir de un objeto Titular
            txtTitulo.setText(t.getTitulo());
            txtSubtitulo.setText(t.getSubtitulo());
            imagen.setImageResource(t.getImagen());
        }
    }

    public AdaptadorTitulares(ArrayList<Titular> datos)
    {
        //El constructor de esta clase
        this.datos = datos;
    }

    @Override
    public TitularesViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        //crea los nuevos objetos ViewHolder necesarios para los elementos de la colección
        //indico que inflo con el  listitem_titular.xml
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.listitem_titular, viewGroup, false);

        itemView.setOnClickListener(this);
        //android:background="?android:attr/selectableItemBackground"

        TitularesViewHolder tvh = new TitularesViewHolder(itemView);

        return tvh;
    }

    @Override
    public void onBindViewHolder(TitularesViewHolder viewHolder, final int pos)
    {
        //Encargado de actualizar los datos de un ViewHolder ya existente
        //Une los items del holder con el recycler view, se llamda cada vez que se infla un item
        //Aca puedo poner los listener de los items, si quiero

        /*
         *Recupera el objeto Titular correspondiente a la posición recibida como
         *parámetro y asigna sus datos sobre el ViewHolder también recibido como parámetro
         */
        Titular item = datos.get(pos);

        viewHolder.bindTitular(item);

        viewHolder.button.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                Toast.makeText(v.getContext(), "Rec_view bttn "+ pos +" pressed!!",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public void setOnClickListener(View.OnClickListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if(listener != null)
            listener.onClick(view);
    }
}